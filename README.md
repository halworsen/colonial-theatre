# Colonial Theatre
This repository contains various bits of code related to processing or visualizing data produced by the CMSS13 round recorder.

The most important bit here is the web application. You'll find that in `colonial-theatre` along with config/deployment instructions.
Big thanks to Atebite for the original work on the code that went into this.
