# Colonial Theatre
The Colonial Theater is a web application for playing back simplified yet information rich recordings of rounds that have been played on the live server. It provides an interactive, big picture view of the round's progression, with features to track individual players and their state at any point in the round.

The Colonial Theatre is split up into a frontend created with React and a Django backend that passes data to the frontend via a REST API. The frontend mostly consists of the actual playback tool, and lies in the `theatre` folder. The backend lies in the `backstage` folder. See each folder for more details

# Configuration & deployment

## Node/React
* Install `react-scripts` with `npm install react-scripts`.
* Compile a static version of the React application that can be served by Django by going to the `theatre` folder, then running `npm run build`.

## Django
* Set up environmental variables:
* - Set `DJANGO_SECRET_KEY` to a long, random string. Django recommends > 50 characters with > 5 unique characters.
* - **Set `DJANGO_DEBUG` to 0.**
* - Update `PYTHONPATH` to include the path to the root web application folder so Python can import the `backstage` module.
* Install requirements with `pip install -r requirements.txt`.
* Configure `ALLOWED_HOSTS` in `settings.py` to reflect where you'll be hosting the application, i.e. add the domain name where you'll be hosting the application.

## Deploying
* Collect the static application produced by the Node build and other static files produced by Django by running `python manage.py collectstatic`. You may get a warning about overwriting files. If so, enter `yes`.

At this point, you may verify that the application has been properly configured by running the application locally with `python manage.py runserver --insecure`. The application should be available at `http://127.0.0.1:8000`.

* Configure your web server to serve the static files collected by Django, they should be located in `colonial-theatre/application/static`.
* The application can be deployed as a WSGI app. This part depends on your preferred web server. See the [Django docs](https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/) for details. Specifically, the server should be setup to serve the static files collected by Django, and run the Django WSGI app on the side.

## Example configurations

Using Apache 2.4 with mod_wsgi, your httpd-vhosts.conf file may end up looking something like this:
```
<VirtualHost *:6969>
    ServerAdmin admin@bigboycmstaff.com
    ServerName localhost:6969

	# Serve static files at /static
    Alias /static "E:/Documents/colonial-theatre/colonial-theatre/application/static"
    <Directory "E:/Documents/colonial-theatre/colonial-theatre/application/static">
        Require all granted
    </Directory>

	# Serve the application at /
    WSGIScriptAlias / "E:/Documents/colonial-theatre/colonial-theatre/backstage/wsgi.py"
    <Directory "E:/Documents/colonial-theatre/colonial-theatre/backstage">
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

	# Make sure the logs directory exists before starting the server
    ErrorLog "E:/Documents/colonial-theatre/colonial-theatre/logs/error.log"
</VirtualHost>
```