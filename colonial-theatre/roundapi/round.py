import json

class Round:
	'''
		A round object represents a full game round and all its tracked participants
	'''

	# All participant histories of the game
	histories = []

	# How many snapshots the round consisted of
	snapshots = 0

	# When the game started
	start = 0
	# When it ended
	end = 0

	# What gamemode was played
	gamemode = "unknown"

	# The map that was played on
	game_map = "unknown"

	# The outcome of the round
	outcome = "unknown"

	def __init__(self, snapshots, start, end, gamemode, game_map, outcome):
		self.snapshots = snapshots
		self.start = start
		self.end = end
		self.gamemode = gamemode
		self.game_map = game_map
		self.outcome = outcome
		
	def add_history(self, history):
		self.histories.append(history)

	def get_snapshots(self, snapshot_no):
		for history in self.histories:
			if len(history.snapshots) <= snapshot_no:
				continue
			yield history.get_snapshot(snapshot_no)

	def participants(self):
		'''
			Provides an iterable over all participants in the round
		'''

		for participant in self.histories:
			yield participant


class History:
	'''
		A history object represents a single player's full tracked history for the round
	'''

	# Which round this history belongs to
	in_round = None

	# Holds all snapshots in this player history
	snapshots = ()

	# Reference to the player, serves as the key for the player info and their recorded history
	ref = ""

	# Player's BYOND key
	key = ""
	# Character name
	name = ""
	# Character faction
	faction = ""
	# Character role
	role = ""
	# What squad they were in, if any
	squad = ""

	def __init__(self, in_round, ref, history_data):
		'''
			history_data should be the full data string containing all of this player's snapshots
		'''

		self.in_round = in_round
		self.ref = ref

		# :-1 because there's a ; at the end of every history, giving an empty entry
		self.snapshots = tuple(Snapshot(snapshot) for snapshot in history_data.split(";")[:-1])

	def set_key(self, key):
		self.key = key

	def set_name(self, name):
		self.name = name

	def set_faction(self, faction):
		self.faction = faction

	def set_role(self, role):
		self.role = role

	def set_squad(self, squad):
		self.squad = squad

	def get_key(self):
		return self.key

	def get_name(self):
		return self.name

	def get_faction(self):
		return self.faction

	def get_role(self):
		return self.role

	def get_squad(self):
		return self.squad

	def get_snapshot(self, snapshot_no):
		'''
			Not all players were in the full round
			But all tracked players are accounted for when it ends
			So if you have less than a full round worth of snapshots, say N,
			they only show the N last snapshots of the game

			This takes that into account and returns None if the history doesn't contain the snapshot
		'''

		# At which snapshot the history starts in the round
		history_start = (self.in_round.snapshots - len(self.snapshots))
		# Can't provide snapshots before the start of the tracked history
		if snapshot_no < history_start:
			return None

		return self.snapshots[snapshot_no - history_start]

class Snapshot:
	'''
		Helper class for storing a single snapshot in a round participant's tracked history
	'''

	# The player's position in the game, given as a 3-tuple
	position = ()
	# The player's stat field
	stat = 0
	# Whether or not the player is incapacitated
	incap = False
	# Damage by type given as a 6-tuple
	# In order: brute, burn, oxy, tox, clone, brain
	damage = ()

	def __init__(self, data):
		'''
			Snapshot takes a comma-separated data string
			Value 1-3 are the player position
			Value 4 is their stat value
			Value 5 is their incap value
			The remainder are damage
		'''

		data = data.split(",")

		self.position = tuple(int(x) for x in data[:3])
		self.stat = data[3]
		self.incap = data[4]
		self.damage = tuple(float(x) for x in data[5:])

	def get_x(self):
		return self.position[0]

	def get_y(self):
		return self.position[1]

	def get_z(self):
		return self.position[2]

	def get_stat(self):
		return self.stat

	def get_incap(self):
		return self.incap

	def get_brute(self):
		return self.damage[0]

	def get_burn(self):
		return self.damage[1]

	def get_oxy(self):
		return self.damage[2]

	def get_tox(self):
		return self.damage[3]

	def get_clone(self):
		return self.damage[4]

	def get_brain(self):
		return self.damage[5]
