from django.apps import AppConfig


class RoundapiConfig(AppConfig):
    name = 'roundapi'
