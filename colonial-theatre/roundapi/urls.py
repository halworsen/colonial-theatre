
from django.urls import path

from .views import RoundListView, RoundDetailView

urlpatterns = [
	path('rounds/', RoundListView.as_view()),
	path('rounds/<pk>', RoundDetailView.as_view())
]
