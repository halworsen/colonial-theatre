import base64
from datetime import datetime, timedelta

from backstage.settings import ROUND_API
from django.http.response import HttpResponseBadRequest
from django.shortcuts import render
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.views import APIView


class RoundListView(APIView):
    '''
        A list of all rounds and their basic information
    '''

    def get(self, request, format=None):
        import os
        import json

        all_rounds_data = []

        # Fetch all recorded rounds on file and construct some basic data about each round
        data_files = os.listdir(f'{ROUND_API["ROUND_DATA_FOLDER"]}/')
        round_id = 1
        for file in data_files:
            if not file.endswith('meta.json'):
                continue

            with open(f'{ROUND_API["ROUND_DATA_FOLDER"]}/{file}', 'r') as data_file:
                data = json.load(data_file)

                # Be convenient about it and provide the round length (in seconds) instead of start/end timestamps
                # Date when the round started is still provided
                start_date = datetime.strptime(data['start'], ROUND_API['BYOND_TIME_FORMAT'])
                end_date = datetime.strptime(data['end'], ROUND_API['BYOND_TIME_FORMAT'])
                delta = end_date - start_date

                round_data = {
                    'rid': round_id,

                    'name': (data['name'] if ('name' in data) else 'unknown'),
                    'gamemode': data['gamemode'],
                    'map': data['map'],
                    'outcome': data['outcome'],

                    'start': data['start'],
                    'end': data['end'],
                    'length': int(delta.total_seconds())
                }

                all_rounds_data.append(round_data)
                round_id += 1

        return Response(all_rounds_data)

class RoundDetailView(APIView):
    '''
        Full information on specific rounds
    '''

    def get(self, request, format=None, pk=-1):
        '''
            Return information about the given round
        '''

        pk = int(pk)
        # not even requesting anything resembling a round
        if pk is None:
            return HttpResponseBadRequest()

        pk -= 1
        if pk < 0:
            return HttpResponseBadRequest()

        import os
        import json

        data_files = os.listdir(f'{ROUND_API["ROUND_DATA_FOLDER"]}/')
        json_files = tuple(file for file in data_files if file.endswith('data.json.bz2'))

        # requesting a round that's not on file
        if pk >= len(json_files):
            return HttpResponseBadRequest()

        requested_file = json_files[pk]
        response = None
        with open(f'{ROUND_API["ROUND_DATA_FOLDER"]}/{requested_file}', 'rb') as data_file:
            # Base 64 encode the compressed round data and send that
            # The client will have to decode and uncompress, essentially saving some network load and offloading processing to the client
            encoded_round_data = base64.b64encode(data_file.read())
            response = Response(encoded_round_data)

        # Didn't find the file
        if not response:
            return HttpResponseServerError()
    
        return response
