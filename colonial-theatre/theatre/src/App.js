import React from 'react';

import LoadingScreen from './components/LoadingScreen.js';
import MapView from './components/MapView.js';
import RoundBrowser from './components/RoundBrowser.js';

import getRounds from './data/RoundData.js';

import './css/app.css';

class App extends React.Component {

	constructor(props) {
		super(props);

		this.roundDatas = undefined;

		this.state = {
			selectedRoundID: -1,
			browsing: true,
			loading: true
		};
	}

	componentDidMount() {
		this.loadRoundListData();
	}

	// Gets the list of rounds available for playback
	async loadRoundListData() {
		this.setState({
			browsing: true,
			loading: true
		})

		this.roundDatas = await getRounds();

		this.setState({ loading: false });
	}

	// This is split up into a load and awaiting function because the RoundData object uses a promise for retrieving the data
	async loadRoundData(roundID) {
		this.setState({
			selectedRoundID: roundID,
			loading: true
		});

		const selectedRound = this.roundDatas.find(data => data.roundID === roundID);
		await selectedRound.getRoundData();

		this.setState({
			browsing: false,
			loading: false
		})
	}

	render() {
		const { selectedRoundID, browsing, loading } = this.state;

		// Show a simple loading screen when while we wait for data from the backend
		return (
			<div className="App">
				{!loading ? (
					browsing ? (
						<RoundBrowser
							roundDatas={this.roundDatas}
							onRoundSelected={(roundID) => this.loadRoundData(roundID)}
						></RoundBrowser>
					) : (
						<MapView
							roundData={this.roundDatas.find(data => data.roundID === selectedRoundID)}
							onReturnToBrowser={() => this.loadRoundListData()}
						></MapView>
					)
				) : (
					<LoadingScreen/>
				)}
			</div>
		);
	}
}

export default App;
