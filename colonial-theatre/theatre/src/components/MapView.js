import React from "react";

import { MapInteractionCSS } from "react-map-interaction";
import ReactTooltip from 'react-tooltip';

import Blob from "./PlayerBlob.js";
import PlayControls from "./PlayControls.js";
import PlayerInfoPanel from "./PlayerInfoPanel.js";

import "../css/mapview.css";

class MapView extends React.Component {
	constructor(props) {
		super(props);

		this.data = props.roundData;

		this.playbackSpeeds = [16, 8, 4, 2, 1];
		this.zoomLevels = [0.25, 0.5, 1, 2, 3];
		this.updatedTracking = false;

		this.lastTick = 0;

		this.state = {
			// Playback related vars
			tick: 0,
			// At which frame did we last update the snapshot
			lastFrame: 0,
			// Whether or not the playback is enabled
			playing: false,
			// How many ticks to wait before showing the next snapshot. See the playbackSpeeds array
			curSpeed: 3,
			// Which snapshot is being displayed
			curSnapshot: 0,

			// Which player we're tracking
			trackingMobID: undefined,

			// Viewport related vars
			showAlmayer: true,
			// Indexes zoomLevels
			curZoom: 2,
			mapTrans: {
				x: -6176,
				y: -4160
			},
			// Size of the map image. Needed for things like player tracking
			mapSize: {
				w: 0,
				h: 0
			}
		};
	}

	componentDidMount() {
		this.interval = setInterval(() => this.doTick(), 50);
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}

	// Ensure the view is always centered on the tracked player
	componentDidUpdate() {
		const { trackingMobID } = this.state;

		if(trackingMobID !== undefined && !this.updatedTracking) {
			this.updatedTracking = true;
			this.updateTrackedView(trackingMobID);
		} else {
			this.updatedTracking = false;
		}
	}

	doTick() {
		const { tick, lastFrame, playing, curSpeed, curSnapshot } = this.state;

		// Don't tick while paused
		if(!playing) {
			return;
		}

		this.setState({ tick: tick + 1 });

		if(tick > (lastFrame + this.playbackSpeeds[curSpeed])) {
			let newSnapshot = curSnapshot + 1;
			let continuePlaying = playing;
			if(newSnapshot > (this.data.getTotalSnapshots() - 1)) {
				continuePlaying = false;
			}

			this.setState({
				playing: continuePlaying,
				lastFrame: tick,
				curSnapshot: Math.min(curSnapshot + 1, this.data.getTotalSnapshots() - 1)
			});
		}
	}

	updateMapDimensions() {
		this.setState({ mapSize: { w: this.mapImageElem.naturalWidth, h: this.mapImageElem.naturalHeight } });
	}

	updateTrackedView(mobID) {
		const { curSnapshot, curZoom, mapSize } = this.state;

		const snapshot = this.data.getSnapshot(mobID, curSnapshot);
		if(!snapshot) {
			return;
		}

		const pos = snapshot.getPos();

		const blobX = ((pos.x - 1) * 32) + 16;
		const blobY = (pos.y * 32) + 16;

		// They're neither on the ground nor on the Almayer. Likely in dropship transit, so just wait for them to land
		if(!snapshot.onAlmayer() && !snapshot.onGround()) {
			return;
		}

		this.setState({
			showAlmayer: snapshot.onAlmayer(),
			mapTrans: {
				x: -(blobX * this.zoomLevels[curZoom]) + window.innerWidth/2,
				// i have no idea why i need the -3*32 but that's what /actually/ centers things
				y: -((mapSize.h - blobY) * this.zoomLevels[curZoom]) + window.innerHeight/2 - 3*32
			}
		})
	}

	// When the map view is panned and/or zoomed
	updateView(scale, translation) {
		this.setState({
			trackingMobID: undefined,

			mapTrans: {
				x: translation.x,
				y: translation.y
			}
		});
	}

	trackPlayer(mobID) {
		this.updateTrackedView(mobID);
		this.setState({ trackingMobID: mobID });
	}

	// When the seekbar is moved
	onSliderUpdate(position) {
		const newSnapshot = Math.floor((position / 100) * this.data.getTotalSnapshots());

		this.setState({ curSnapshot: newSnapshot });
	}

	// Toggles playback on and off
	togglePlaying() {
		const { lastFrame, playing, curSnapshot } = this.state;

		// Starting playback at the end of the round starts it over
		let newSnapshot = curSnapshot;
		let newFrame = lastFrame;
		if(!playing && (curSnapshot === (this.data.getTotalSnapshots() - 1))) {
			newSnapshot = 0;
			newFrame = 0;
		}

		this.setState({
			lastFrame: newFrame,
			playing: !playing,
			curSnapshot: newSnapshot
		});
	}

	// Go 1 frame/snapshot forward
	nextFrame() {
		const { curSnapshot } = this.state;

		const newSnapshot = Math.min(curSnapshot + 1, this.data.getTotalSnapshots() - 1);
		this.setState({
			playing: false,
			curSnapshot: newSnapshot
		});
	}

	prevFrame() {
		const { curSnapshot } = this.state;

		const newSnapshot = Math.max(curSnapshot - 1, 0);
		this.setState({
			playing: false,
			curSnapshot: newSnapshot
		});
	}

	// Toggle between the Almayer/game map
	toggleZLevel() {
		this.setState({
			trackingMobID: undefined,
			showAlmayer: !this.state.showAlmayer
		});
	}

	// Cycles through different playback speeds
	cycleSpeed(forwards) {
		// + this.playbackSpeeds.length ensures the result of the modulus is >= 0
		this.setState({ curSpeed: (this.state.curSpeed + (forwards ? 1 : -1) + this.playbackSpeeds.length) % this.playbackSpeeds.length });
	}

	// Cycles through different zooms
	cycleZoom(forwards) {
		const { mapTrans, curZoom } = this.state;

		const newZoom = (curZoom + (forwards ? 1 : -1) + this.zoomLevels.length) % this.zoomLevels.length;
		const oldScale = this.zoomLevels[curZoom];
		const newScale = this.zoomLevels[newZoom];
		const scaleRatio = newScale / (oldScale !== 0 ? oldScale : 1);

		const focalPtDelta = {
		  x: ((window.innerWidth/2) * scaleRatio) - window.innerWidth/2,
		  y: ((window.innerHeight/2) * scaleRatio) - window.innerHeight/2
		};

		const newTranslation = {
		  x: (mapTrans.x * scaleRatio) - focalPtDelta.x,
		  y: (mapTrans.y * scaleRatio) - focalPtDelta.y
		};

		this.setState({
			mapTrans: {
				x: newTranslation.x,
				y: newTranslation.y
			},
			curZoom: newZoom
		})
	}

	render() {
		const { playing, curSpeed, curSnapshot, showAlmayer, mapTrans, curZoom, trackingMobID } = this.state;
		const totalSnapshots = this.data.getTotalSnapshots();

		// Grab the game history data for this snapshot
		const snapData = [];
		for(const mobID in this.data.getMobIDs()) {
			const playerData = this.data.getPlayerData(mobID);
			const snapshot = this.data.getSnapshot(mobID, curSnapshot);

			if(!snapshot) {
				continue;
			}

			if((showAlmayer && !snapshot.onAlmayer()) || (!showAlmayer && !snapshot.onGround()) ) {
				continue;
			}

			const pos = snapshot.getPos();
			snapData.push({
				id: mobID,
				x: pos.x,
				y: pos.y,
				stat: snapshot.getStat(),
				incap: snapshot.isIncapacitated(),
				name: playerData.getName(),
				key: playerData.getKey(curSnapshot),
				faction: playerData.getFaction(),
				role: playerData.getRole(),
				squad: playerData.getSquad(),
				damage: snapshot.getDamages()
			});
		}

		const map_name = (showAlmayer ? "almayer" : this.data.getMap());

		let playerInfoPanel = undefined;
		if(trackingMobID !== undefined) {
			const trackedData = this.data.getPlayerData(trackingMobID);
			const snapshot = this.data.getSnapshot(trackingMobID, curSnapshot);

			// god i wanted to use early return so much here but it FUCKS SHIT UP
			if(snapshot) {
				playerInfoPanel = (
					<PlayerInfoPanel
						name={trackedData.getName()}
						role={trackedData.getRole()}
						squad={trackedData.getSquad()}
						damage={snapshot.getDamages()}
					>
					</PlayerInfoPanel>
				);
			}
		}

		return (
			<div className="mapView">
				{/* The heavy lift is here. This is the interactive map housing all the player blobs */}
				<MapInteractionCSS
					scale={this.zoomLevels[curZoom]}
					translation={mapTrans}
					onChange={({scale, translation}) => this.updateView(scale, translation)}

					disableZoom={true}
				>
					<img
						className={"mapImage " + ((this.zoomLevels[curZoom] > 1) ? "crisp" : "pixelated")}
						src={require("../rsc/" + map_name + ".png")}
						alt=""

						ref={(elem) => { this.mapImageElem = elem; }}
						onLoad={() => this.updateMapDimensions()}
					/>

					{snapData.map((snap) =>
							<Blob
								key={snap.id}

								id={snap.id}
								name={snap.name}
								bkey={snap.key}
								x={snap.x}
								y={snap.y}
								stat={snap.stat}
								incapped={snap.incap}
								faction={snap.faction}
								role={snap.role}
								squad={snap.squad}

								scale={this.zoomLevels[curZoom]}

								onClicked={(ref) => this.trackPlayer(ref)}
							>
							</Blob>
						)}
				</MapInteractionCSS>

				{/* Tooltips need to be placed outside the map interaction element, or they'll get scaled along with everything else */}
				<div>
					{snapData.map((snap) =>
						<ReactTooltip key={snap.id} id={snap.id} className="playerTooltip"
							multiline={false}
						>
							<p className="playerTooltipText">{snap.name}</p>
						</ReactTooltip>
					)}
				</div>

				{/* Controls menu at the bottom */}
				<PlayControls
					className="playbackControls"
					onSlide={(position) => this.onSliderUpdate(position)}
					onPlayToggle={() => this.togglePlaying()}
					onPrevFrame={() => this.prevFrame()}
					onNextFrame={() => this.nextFrame()}
					onCycleSpeed={(forwards) => this.cycleSpeed(forwards)}
					onCycleZoom={(forwards) => this.cycleZoom(forwards)}
					onZToggle={() => this.toggleZLevel()}
					onReturnToBrowser={() => this.props.onReturnToBrowser()}

					seekVal={(curSnapshot / totalSnapshots) * 100}
					totalSnapshots={totalSnapshots}
					playing={playing}
					playbackSpeed={curSpeed}
					zName={showAlmayer ? "ALM" : "GRD"}
					zoomLevel={this.zoomLevels[curZoom]}
				>
				</PlayControls>

				{/* Not much of a panel. It's just text with the round name & map */}
				<div className="roundInfoPanel">
					<h2 className="roundTitle">{this.data.getRoundName()}</h2>
					<p className="roundMap">{this.data.getMap()}</p>
				</div>

				{playerInfoPanel}
			</div>
		);
	}
}

export default MapView;
