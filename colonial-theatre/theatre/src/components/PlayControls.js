import React from 'react';
import Slider from 'rc-slider';
import Tooltip from 'rc-tooltip';
import 'rc-slider/assets/index.css';

import '../css/playback.css';

const Handle = Slider.Handle;

function PlayControls(props) {
	return (
		<div className="playbackControls noselect">
			{/* Seekbar */}
			<Slider
				className="seekBar"
				step={0.1}
				onChange={(position) => props.onSlide(position)}
				value={props.seekVal}
				handle={(handleProps) => {
					const { value, dragging, index, ...restProps } = handleProps;

					const seconds = Math.floor((value/100)*props.totalSnapshots*5);

					let secs = seconds % 60;
					secs = secs < 10 ? ("0" + secs) : secs;
					let mins = Math.floor(seconds/60) % 60;
					mins = mins < 10 ? ("0" + mins) : mins;
					let hours = 12 + Math.floor(seconds/3600) % 24;

					return (
						<Tooltip
							prefixCls="rc-slider-tooltip"
							overlay={hours + ":" + mins + ":" + secs}
							visible={dragging}
							placement="top"
							key={"seek" + value}
						>
							<Handle value={value} {...restProps}/>
						</Tooltip>
					);
				}}
			/>

			{/* Left-justified button for returning to the round browser landing page */}
			<div className = "returnButtonHolder">
				<div className="controlButton"
					onClick={() => props.onReturnToBrowser()}
				>
					<div className="returnIcon"></div>
				</div>
				<p className="buttonTitle">BACK</p>
			</div>

			{/* Playback controls */}
			<div className="controls">
				<div
					className="controlButton"
					onClick={() => props.onPrevFrame()}
				>
					<div className="prevFrameArrow"></div>
					<div className="prevFrameBlock"></div>
				</div>

				<div
					className="controlButton"
					onClick={() => props.onPlayToggle()}
				>
					<div className={props.playing ? "pauseIcon" : "playIcon"}></div>
				</div>

				<div
					className="controlButton"
					onClick={() => props.onNextFrame()}
				>
					<div className="nextFrameBlock"></div>
					<div className="nextFrameArrow"></div>
				</div>
			</div>

			{/* Other controls like z toggle, playback speed and zoom */}
			<div className="altControls">
				<div className="controlButtonHolder">
					<p className="buttonTitle">Z LEVEL</p>
					<div
						className="controlButton"
						onClick={() => props.onZToggle()}
					>
						<p>{props.zName}</p>
					</div>
				</div>

				<div className="controlButtonHolder">
					<p className="buttonTitle">SPEED</p>
					<div
						className="controlButton"
						onClick={() => props.onCycleSpeed(true)}
						onContextMenu={(event) => {
							props.onCycleSpeed(false);
							event.preventDefault();
						}}
					>
						<p>x{Math.pow(2, props.playbackSpeed)}</p>
					</div>
				</div>

				<div className="controlButtonHolder">
					<p className="buttonTitle">ZOOM</p>
					<div
						className="controlButton"
						onClick={() => props.onCycleZoom(true)}
						onContextMenu={(event) => {
							props.onCycleZoom(false);
							event.preventDefault();
						}}
					>
						<p>x{props.zoomLevel}</p>
					</div>
				</div>
			</div>
		</div>
	);
}

export default PlayControls;
