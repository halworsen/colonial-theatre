import React from "react";
import '../css/playerinfopanel.css';

function PlayerInfoPanel(props) {
	return (
		<div className="playerInfoPanel noselect">
			<h3 className="playerName">{props.name}</h3>
			{/* Display the squad name before the role if they're in a squad */}
			<p className="playerRole">{props.squad !== "none" ? props.squad + " " + props.role : props.role}</p>

			<table className="damageTable">
				<thead>
					<tr className="damageIcons" align="center">
						<th><img src={require("../rsc/icons/brute.png")} alt=""/></th>
						<th><img src={require("../rsc/icons/burn.png")} alt=""/></th>
						<th><img src={require("../rsc/icons/tox.png")} alt=""/></th>
						<th><img src={require("../rsc/icons/oxy.png")} alt=""/></th>
					</tr>
				</thead>

				<tbody>
					<tr className="damageValues">
						<td><span>{props.damage.brute}</span></td>
						<td><span>{props.damage.burn}</span></td>
						<td><span>{props.damage.tox}</span></td>
						<td><span>{props.damage.oxy}</span></td>
					</tr>
				</tbody>
			</table>
		</div>
	);
}

export default PlayerInfoPanel;