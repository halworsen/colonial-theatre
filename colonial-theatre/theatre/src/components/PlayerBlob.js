import React from "react";
import config from "../config/config.json";

import '../css/playerblob.css';

function Blob(props) {
	return (
		<div className="noselect">
			{/*
				Fun note: BYOND's origins are (1,1), not (0,0). That's why we do props.x - 1. But we don't do props.y - 1
				Why? Because the icon is placed directly below the map in the DOM, and since the icons are 32x32 px,
				the "origin" for this system becomes (0, -1). So it cancels out!
			*/}
			<div
				className="playerBlob"
				style={{
					transform: "translate(" + String((props.x - 1)*32) + "px, -" + String(props.y*32 + 4) + "px)"
				}}
				data-tip data-for={props.id}

				onClick={() => props.onClicked(props.id)}
			>

				{/* Background color, blob icon and color filter depends on faction, stat and squad */}
				<img
					alt=""
					src={require("../rsc/icons/" + (config.roleImages[props.role] ? config.roleImages[props.role] : config.roleImages["default"]) + ".png")}
					style={{
						background: (props.faction === "USCM" ? config.colors[props.squad] : config.colors[props.faction]),
						"borderRadius": "16px",
						filter: config.statFilters[props.stat] + (props.incapped ? " blur(.2px)" : "")
					}}
				/>

				{/* Only show the overlay icon if they're unconscious or dead */}
				{props.stat !== "0" &&
					<img
						className="overlay"
						alt=""
						src={require("../rsc/icons/" + (config.statIcons[props.stat]) + ".png")}
					/>
				}
			</div>
		</div>
	);
}

export default Blob;
