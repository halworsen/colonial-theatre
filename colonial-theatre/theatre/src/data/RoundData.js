import axios from 'axios';
import base64js from 'base64-js';
import bzip2 from 'bzip2';

class RoundData {
	constructor(metaData) {
		this.roundID = metaData.rid;
		this.data = metaData;
		this.ready = false;
	}

	/*
	 * Fetches full round data
	 */
	async getRoundData() {
		const fullDataRes = await axios.get('/api/rounds/' + String(this.roundID) + '?format=json');
		const fullData = fullDataRes.data;

		// The server returns base64 encoded bytes from a bzipped file
		// This decodes and uncompresses those bytes into JSON round data
		// Decode to a byte array
		const bytes = base64js.toByteArray(fullData);
		// Decompress to the json bytes
		const roundDataBytes = bzip2.simple(bzip2.array(bytes));
		// Decode into a string and parse it as JSON
		const roundData = JSON.parse(new TextDecoder().decode(roundDataBytes));

		this.data = {...this.data, ...roundData};
		this.ready = true;
	}

	getMobIDs() {
		return this.data["player_infos"];
	}

	getTotalSnapshots() {
		return this.data["history_meta"]["snapshots"];
	}

	getRoundName() {
		return this.data["game_info"]["name"];
	}

	getMap() {
		return this.data["game_info"]["map"];
	}

	getPlayerData(mobID) {
		return new PlayerData(mobID, this.data["player_infos"][mobID]);
	}

	getSnapshot(mobID, snapshot) {
		const playerData = this.getPlayerData(mobID);
		const historyEnd = (playerData.getEndSnap() === -1 ? this.getTotalSnapshots() : playerData.getEndSnap()) - 1;
		if((snapshot < playerData.getStartSnap()) || (snapshot >= historyEnd)) {
			return undefined;
		}

		const fullSnapStr = this.data["player_history"][mobID];
		const snapDataStrs = fullSnapStr.split(";");
		const index = (snapshot - playerData.getStartSnap());

		return new Snapshot(mobID, snapDataStrs[index]);
	}
}

class PlayerData {
	constructor(mobID, playerData) {
		this.mobID = mobID;

		this.keys = playerData["keys"];
		this.name = playerData["name"];
		this.faction = playerData["faction"];
		this.role = playerData["role"];
		this.squad = playerData["squad"];

		this.startSnap = playerData["start_snapshot"];
		this.endSnap = playerData["end_snapshot"];
	}

	// Gets the key of the player controlling the mob at the given snapshot
	getKey(snapshot) {
		let lastKeySnap = this.keys[0];
		let key = this.keys[1];

		for(let index = 0; index < this.keys.length; index += 2) {
			const keySnap = this.keys[index];
			const snappedKey = this.keys[index + 1];

			if(keySnap < lastKeySnap || keySnap > snapshot) {
				continue;
			}

			if(keySnap > lastKeySnap) {
				lastKeySnap = keySnap;
				key = snappedKey;
			}
		}

		return key;
	}

	getName() {
		return this.name;
	}

	getFaction() {
		return this.faction;
	}

	getRole() {
		return this.role;
	}

	getSquad() {
		return this.squad;
	}

	getStartSnap() {
		return this.startSnap;
	}

	getEndSnap() {
		return this.endSnap;
	}
}

class Snapshot {
	constructor(mobID, snapData) {
		this.mobID = mobID;

		snapData = snapData.split(",");

		this.x = Number(snapData[0]);
		this.y = Number(snapData[1]);
		this.z = Number(snapData[2]);

		this.stat = snapData[3];
		this.incap = Boolean(Number(snapData[4]));

		this.damage = {
			brute: Math.round(Number(snapData[5])),
			burn: Math.round(Number(snapData[6])),
			tox: Math.round(Number(snapData[7])),
			oxy: Math.round(Number(snapData[8])),
			clone: Math.round(Number(snapData[9])),
			brain: Math.round(Number(snapData[10]))
		}
	}

	onAlmayer() {
		return (this.z === 3);
	}

	onGround() {
		return (this.z === 1);
	}

	getPos() {
		return { x: this.x, y: this.y, z: this.z };
	}

	getDamages() {
		return this.damage;
	}

	getDamage(type) {
		return this.damage[type];
	}

	getStat() {
		return this.stat;
	}

	isIncapacitated() {
		return this.incap;
	}
}

const roundDataObjects = [];

async function getRounds() {
	// Clear out the round data array
	roundDataObjects.length = 0;

	const res = await axios.get('/api/rounds/?format=json');
	const data = res.data;
	for (let i = 0; i < data.length; i++) {
		const roundData = new RoundData(data[i]);
		roundDataObjects.push(roundData);
	}

	return roundDataObjects;
}


export default getRounds;