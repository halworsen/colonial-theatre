import json
from math import floor
from time import sleep
from threading import Thread

import tkinter as tk
from PIL import Image, ImageTk

from round.history import History, Snapshot
from round.round import Round

COLORS = {
	"Alpha": "#D83148",
	"Bravo": "#D8B63A",
	"Charlie": "#D147D8",
	"Delta": "#4958D8",
	"Xeno": "#9042FF"
}

class CTApp:

	# Various tkinter objects
	root = None
	almayer_canvas = None
	map_canvas = None
	time_label_text = None
	scoller = None

	# The PIL Image of the Almayer
	almayer_image = None
	# The TkInter friendly version of the Almayer image
	almayer_tk_image = None
	# Map image dimensions
	almayer_width = 0
	almayer_height = 0

	# The PIL Image of the map
	map_image = None
	# The TkInter friendly version of the map image
	map_tk_image = None
	# Map image dimensions
	map_width = 0
	map_height = 0

	# All player blobs on the map
	canvas_blobs = []

	# Current map
	round = None

	# Which snapshot is being viewed
	current_snapshot = 0

	# Whether or not the playback is active
	playback_on = False

	def __init__(self):
		self.root = tk.Tk()
		self.root.title("Colonial Theatre")

		# Parse round data
		data = None
		with open("rsc/game0.json", "r") as file:
			data = json.load(file)

		# Setup the round object
		meta = data["history_meta"]
		general_info = data["game_info"]
		player_infos = data["player_infos"]
		player_history = data["player_history"]

		self.round = Round(meta["snapshots"], meta["start"], meta["end"], general_info["gamemode"], general_info["map"], general_info["outcome"])
		# Construct each player's history
		for ref in player_infos:
			player_history = History(self.round, ref, data["player_history"][ref])
			info = player_infos[ref]

			player_history.set_key(info["key"])
			player_history.set_name(info["name"])
			player_history.set_faction(info["faction"])
			player_history.set_role(info["role"])
			player_history.set_squad(info["squad"])

			# Add the history to the round
			self.round.add_history(player_history)

		self.populate()
		self.update_snap("startup", 0)

	def start(self):
		self.root.mainloop()

	def populate(self):
		# Wrap the maps in a panel
		map_panel = tk.Label(self.root)

		# Setup the map images/backgrounds
		self.almayer_image = Image.open("rsc/almayer.png")
		self.almayer_width, self.almayer_height = self.almayer_image.size

		self.map_image = Image.open(f"rsc/{self.round.round_map}.png")
		self.map_width, self.map_height = self.map_image.size

		# Make them grayscale
		self.almayer_image = self.almayer_image.convert("L").convert("RGB")
		self.map_image = self.map_image.convert("L").convert("RGB")
		# Scale them down to a nice size
		self.almayer_image = self.almayer_image.resize((self.almayer_width // 4, self.almayer_height // 4), Image.NEAREST)
		self.almayer_width, self.almayer_height = self.almayer_image.size

		self.map_image = self.map_image.resize((self.map_width // 4, self.map_height // 4), Image.NEAREST)
		self.map_width, self.map_height = self.map_image.size

		self.almayer_tk_image = ImageTk.PhotoImage(self.almayer_image)
		self.almayer_canvas = tk.Canvas(map_panel, width=self.almayer_width, height=self.almayer_height)
		self.almayer_canvas.create_image(0, 0, anchor="nw", image=self.almayer_tk_image)

		self.map_tk_image = ImageTk.PhotoImage(self.map_image)
		self.map_canvas = tk.Canvas(map_panel, width=self.map_width, height=self.map_height)
		self.map_canvas.create_image(0, 0, anchor="nw", image=self.map_tk_image)

		self.almayer_canvas.pack(side="left", expand=True, fill="both")
		self.map_canvas.pack(side="right", expand=True, fill="both")

		# Bottom info panel
		info_panel = tk.Label(self.root)

		# Game info
		info_ss = tk.Label(info_panel, text=f"Snapshots: {self.round.snapshots}")
		info_ss.grid(row=0, column=0, sticky="w")
		info_gm = tk.Label(info_panel, text=f"Gamemode: {self.round.gamemode}")
		info_gm.grid(row=1, column=0, sticky="w")
		info_map = tk.Label(info_panel, text=f"Map: {self.round.round_map}")
		info_map.grid(row=2, column=0, sticky="w")
		info_result = tk.Label(info_panel, text=f"Outcome: {self.round.outcome}")
		info_result.grid(row=3, column=0, sticky="w")

		playback_button = tk.Button(info_panel, text="Play", command=self.start_playback)
		playback_button.grid(row=4, column=0, sticky="w")

		self.time_label_text = tk.StringVar()
		self.time_label_text.set(f"Round time (approximate): {self.get_round_time(0)}")
		info_time = tk.Label(info_panel, textvariable=self.time_label_text)
		info_time.grid(row=5, column=0, sticky="w")

		# Scrollbar for selecting snapshot
		self.scroller = tk.Scrollbar(self.root, orient="horizontal")
		self.scroller.config(command=self.update_snap)

		map_panel.pack(side="top", expand=True, fill="both")
		info_panel.pack(side="bottom", fill="x")
		self.scroller.pack(side="bottom", fill="x")

	def get_round_time(self, snapshot_no):
		'''
			Returns the (approximate) round time at the given snapshot
			Given as a nicely formatted string
		'''

		# Snapshots are taken every ~5 seconds
		seconds_full = snapshot_no * 5
		seconds = seconds_full % 60
		minutes = (seconds_full // 60) % 60
		# Roundstart is 12:00
		hours = 12 + (seconds_full // 3600)

		if seconds < 10:
			seconds = f"0{str(seconds)}"
		if minutes < 10:
			minutes = f"0{str(minutes)}"

		return f"{hours}:{minutes}:{seconds}"

	def clear_canvas(self):
		'''
			Clears player blobs off the canvases
		'''

		self.almayer_canvas.delete("blob")
		self.map_canvas.delete("blob")

	def update_snap(self, *args):
		if args[0] == "moveto":
			self.playback_on = False

		frac = float(args[1])
		if frac < 0:
			return

		self.clear_canvas()
		self.current_snapshot = floor(frac * (self.round.snapshots - 1))

		for player in self.round.participants():
			snap = player.get_snapshot(self.current_snapshot)
			if not snap:
				continue

			canvas = None
			height = 0
			if snap.get_z() == 3:
				canvas = self.almayer_canvas
				height = self.almayer_height

			if snap.get_z() == 1:
				canvas = self.map_canvas
				height = self.map_height

			if not canvas:
				continue

			color = "#64D791"
			squad = player.get_squad()
			if squad != "none":
				color = COLORS[squad]
			if player.get_faction() == "Xenomorph":
				color = COLORS["Xeno"]

			# BYOND has origin at the lower left corner of the map
			minx, miny = (snap.get_x()-1)*4, (height - (snap.get_y())*4)
			blob = canvas.create_rectangle(minx, miny + 4, minx + 4, miny, fill=color, width=0, tag="blob")
			canvas.lift(blob)

		self.time_label_text.set(f"Round time (approximate): {self.get_round_time(self.current_snapshot)}")

		self.scroller.set(frac, frac+0.01)

	def start_playback(self):
		self.playback_on = True
		t = Thread(target=self.playback)
		t.start()

	def playback(self):
		snap = self.current_snapshot
		while snap < self.round.snapshots and self.playback_on:
			self.update_snap("playback", float(snap) / float(self.round.snapshots - 1))
			snap += 1
			sleep(0.1)

if __name__ == "__main__":
	APP = CTApp()
	APP.start()
